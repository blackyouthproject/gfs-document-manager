<?php
/*
Plugin Name: GenForward Survey Document Manager
Description: This plugin manages the documents and data on GenForwardSurvey.com
Author: The Site Clinic
Author URI: https://thesiteclinic.com
Text Domain: gfs-doc
Version: 0.7
*/

if (defined('WP_SITEPATH')) {
    require WP_SITEPATH . '/vendor/autoload.php';
} else {
    require 'vendor/autoload.php';
}

require_once("src/Autoloader.php");
function ddd($m)
{
    var_dump($m);
    die();
}

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

add_action( 'the_post', function( $post, \WP_Query $q )
{
    if( $q->is_search() && $q->is_main_query() && 0 === $q->current_post && $_GET['gds'] == '1'  )
    {
        do_action( 'gfs_docs_search' );
    }
}, 10, 2 );

add_action( 'pre_get_posts', 'gfs_search_filter' );
function gfs_search_filter($query)
{
    if (
        !is_admin()
        && $query->is_main_query()
        && $query->is_search
        && $_GET['gds'] == '1'
    )
    {
        $value = 'what';
        $query->gfs = ['documents' => gfs_custom_search($_GET['s'])];
    }
    return;
}
function hasGfsSearchResults()
{
    global $wp_query;
    return isset($wp_query->gfs) && count($wp_query->gfs['documents'][0]) > 0;
}
function gfs_custom_search($string)
{
    global $wpdb;
    $string = explode(' ', $string);
    $query = "SELECT * FROM `byp_gf_gfs_documents` WHERE ";
    if (isset($_GET['sj']) && $_GET['sj'] === '1') {
        $documents = gfs_docs_get($_GET['s'], true);
        return [$documents, 'query' => $query];
    }
    foreach ($string as $s) {
        if (strlen($s) > 2)
        {
            $query .= "MATCH (text) AGAINST ('$s' IN BOOLEAN MODE);";
        }
    }
    $documents = $wpdb->get_results( $query );
    return [$documents, 'query' => $query];
}

function subjectSearch($value='')
{
    global $wpdb;
    global $wp_query;
    $string = $_GET['s'];
    if (isset($_GET['sj']) && ($_GET['sj'] == '1') ) {
        $subjectSearch = true;
        $subject = trim($_GET['s']);
        return $documents = gfs_docs_get($subject,true);
    }
}

function print_doc_result( $document )
{ ?>
<style type="text/css">
    .ui.items .item
    {
        margin-bottom: 55px;
        color: #565656;
    }
</style>
        <div class="<?php echo strtolower(gfs_press_month(get_post_meta( $document->ID, 'gfs_press_clipping_month', true ))) ?> <?php echo strtolower($document->type) ?> y-<?php echo strtolower($document->year) ?> m-<?php echo strtolower($document->month) ?> item">
            <div class="ui tiny image doctype">
                <span class="day"><?php echo $document->type ?></span>
            </div>
            <div class="middle aligned content">
                <h3 class="header">
                    <?php echo $document->title ?>
                </h3>
                <p class="excerpt">
                    <?php echo get_excerpt($document); ?>
                </p>
                    <a href="<?php echo wp_get_attachment_url( $document->wp_id ); ?>" class="outboud-link">
                        <i class="external icon"></i>
                    </a>
            </div>
        </div>
<?php }

function gfs_docs_setup_menu(){
    add_menu_page( 'GenForward Document Manager Page', 'GenForward Document Manager', 'manage_options', 'gfs-doc-manager', 'gfs_init' );
}

function gfs_init(){
    display_header();
    // First check if the file appears on the _FILES array
    if(isset($_FILES['test_upload_pdf'])) {
        handle_document();
    }
    else if(isset($_POST['gfs_doc-updated'])) {
        handle_edits(intval($_POST['gfs_doc-updated']));
    }
    else if(isset($_POST['gfs_doc-delete'])) {
        handle_delete(intval($_POST['gfs_doc-delete_id']));
    }
    else if(isset($_GET['edit'])) {
        edit_document(intval($_GET['edit']));
    }
    else {
?>
    <div class="screen-header">
        <h1 class="screen-title">GenForward Survey Document Manager</h1>
        <h2 class="screen-directive">Upload a File</h2>
    </div>
    <!-- Form to handle the upload - The enctype value here is very important -->
    <form method="post" enctype="multipart/form-data">
        <input type='file' id='test_upload_pdf' name='test_upload_pdf'></input>
        <input name="submit" id="submit" class="button button-primary" value="Upload" type="submit">
    </form>
<?php 
    display_all();
}
    
    display_footer();
}
function gfs_docs_get_all($withText = false)
{
    global $wpdb;
    $query = "SELECT id,title,month,year,type,wp_id FROM `byp_gf_gfs_documents` WHERE `deleted_at` = '0000-00-00 00:00:00'";
    if ($withText) {
        $query = "SELECT id,title,month,year,type,text,wp_id FROM `byp_gf_gfs_documents` WHERE `deleted_at` = '0000-00-00 00:00:00'";
    }
    $documents = $wpdb->get_results( $query );
    return $documents;
}

function gfs_sort_documents($documents)
{
    // sort the array of documents by year and then by month
    $year  = array_column($documents, 'year');
    $month = array_column($documents, 'month');
    array_multisort($year, SORT_DESC, $month, SORT_DESC, $documents);
    
    // fin!
    return $documents;
}

function gfs_docs_get_all_publications($withText = false)
{
    $api = new API();
    $documents = $api->get('1,3,4,9');
    $documents = $documents['body']['documents'];
    return gfs_sort_documents($documents);
}

function gfs_docs_get_doc_type($type_id)
{
    $types = [
        'document',
        'report',
        'dataset',
        'factsheet',
        'byp-report',
        'press-release',
        'topline',
        'codebook',
        'field-report',
        'slide-deck'
    ];

    return $types[$type_id];
}
function gfs_docs_get_press_releases()
{
    $api = new API();
    $documents = $api->get('5');
    return $documents['body']['documents'];
}

function gfs_request_document($request)
{
    $id = intval($request['id']);
    if(!$id) return;
    // first, create a new requester
    $data = [
        'first_name'    => $request['first'],
        'last_name'    => $request['last'],
        'email'    => $request['email'],
        'institution'    => $request['institution'],
        'purpose'    => $request['purpose'],
    ];
    if ($request['title']) {
        $data['title'] = $request['title'];
    }
    $api = new API();
    $url = 'requester';
    $requester = $api->post($url, $data);

    if ($requester['success'] === false)
        return $requester;

    // now create the download request
    $data = [
        'requester' => $requester['body']['id'],
        'document' => $id,
    ];
    $url = 'download-request';
    $response = $api->post($url, $data);
    
    // then, approve the download request
    if (!empty($response['body'])) {
        $id = $response['body']['id'];
        $url = "download-request/approve/$id";
        $response = $api->post($url, $data);
    }
	
    return $response;
}

function gfs_get_post_var($var, $default = null)
{
    if (isset($_POST[$var]) && !empty($_POST[$var])) {
        return $_POST[$var];
    }
    return $default;
}
function gfs_get_get_var($var, $default = null)
{
    if (isset($_GET[$var]) && !empty($_GET[$var])) {
        return $_GET[$var];
    }
    return $default;
}
function gfs_docs_get_download_url($id)
{
    return API_URL . '/download/' . $id . '/?f=true';
}
function gfs_docs_get($cat, $withText=false)
{
    $api = new API();
    $documents = $api->get($cat);
    
//     return $documents['body']['documents'];
    return gfs_sort_documents($documents['body']['documents']);

    global $wpdb;
    if (is_array($cat)) {
        $cats = '';
        foreach ($cat as $key => $category) {
            $connector = $key == 0 ? 'AND (' : 'OR';
            $cats .= ' '.$connector. " `type` = '".$category."' ";
        }
        $cats .= ')';
    } else {
        $cats = "AND `type` = '$cat'";
    }
    $query = "SELECT id,title,month,year,type,excerpt,wp_id FROM `byp_gf_gfs_documents` WHERE `deleted_at` = '0000-00-00 00:00:00'$cats";
    if ($withText) {
        $query = "SELECT id,title,month,year,type,excerpt,text,wp_id FROM `byp_gf_gfs_documents` WHERE `deleted_at` = '0000-00-00 00:00:00'$cats";
    }
    $documents = $wpdb->get_results( $query );
    return $documents;
}

function display_all()
{
    global $wpdb;
    $query = "SELECT * FROM `byp_gf_gfs_documents` WHERE `deleted_at` = '0000-00-00 00:00:00'";
    $documents = $wpdb->get_results( $query );
    ?>
    <style type="text/css">
        .alldocs
        {
            margin-top: 75px;
        }
        .alldocs table
        {
            width: 90%;
            margin: 0 auto;
            padding: 30px;
            border-collapse:collapse;
        }
        .alldocs table th
        {
            text-transform: uppercase;
            font-weight: bold;
        }
        .alldocs table tr:nth-child(even) {
            background-color: #dedede;
        }
    </style>
    <div class="alldocs">
    <div class="screen-header">
        <h1 class="screen-title">Uploaded Documents</h1>
    </div>
    <table class="widefat fixed">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Month</th>
                <th>Year</th>
                <th>Type</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($documents as $doc): ?>
            <tr>
                <td><?php echo $doc->id ?></td>
                <td><?php echo $doc->title ?></td>
                <td><?php echo $doc->month ?></td>
                <td><?php echo $doc->year ?></td>
                <td><?php echo $doc->type ?></td>
                <td><a href="<?php echo site_url('wp-admin/admin.php?page=gfs-doc-manager&edit='.$doc->id);?>">Edit</a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
<?php }
function handle_delete($id)
{
    global $wpdb;
    $document = get_document(intval($id));
    $wpdb->query('START TRANSACTION');
    $result1 =
    $wpdb->update( 
        'byp_gf_gfs_documents', 
        array(
            'deleted_at' => date('Y-m-d G:i:s'),
        ), 
        array( 'id' => intval($id) ), 
        array(
            '%s'
        ), 
        array( '%d' ) 
    );
    if($result1) {
        $wpdb->query('COMMIT'); // if you come here then well done

        wp_delete_post( $document->wp_id );
        display_deleted();
    }
    else {
        $wpdb->query('ROLLBACK'); // // something went wrong, Rollback
        display_error('deleting');
    }  
}
function handle_edits( $id )
{
    global $wpdb;
    $wpdb->query('START TRANSACTION');
    $result1 =
    $wpdb->update( 
        'byp_gf_gfs_documents', 
        array(
            'title' => $_POST['gfs_doc-title'],
            'excerpt' => $_POST['gfs_doc-excerpt'],
            'month' => $_POST['gfs_doc-month'],
            'year' => $_POST['gfs_doc-year'],
            'keyword' => $_POST['gfs_doc-keyword'],
            'type' => $_POST['gfs_doc-type'],
            'updated_at' => date('Y-m-d G:i:s'),
        ), 
        array( 'id' => intval($id) ), 
        array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
        ), 
        array( '%d' ) 
    );
    if($result1) {
        $wpdb->query('COMMIT'); // if you come here then well done
        if ($_POST['gfs_doc-type'] === 'press-release') {
            insert_press_release($id);
        } elseif ($_POST['gfs_doc-type'] === 'dataset') {
            create_dataset_package($id);
        }
        edit_document($id);
    }
    else {
        $wpdb->query('ROLLBACK'); // // something went wrong, Rollback
        display_error();
    }  
}

function insert_press_release($doc)
{
    // get insert the press release into the 
    // wtf was i tryna accomplish here -____-
}
function create_dataset_package($id) {
    global $wpdb;
    $query = "SELECT *  FROM `byp_gf_gfs_documents` WHERE `id` = ".$id;
    $document = $wpdb->get_row( $query );
    if(!is_null($document->byp_dwnld_id))
        return;

    $message = <<<_EOM_

    Your data request from GenForwardSurvey.com has been approved. Visit this link to download your data:
    <a href="[DATAURL]">[DATAURL]</a>

_EOM_;

    $request = array(
        'post_title'    => $document->title,
        'post_content'  => $document->excerpt,
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'byp_dwnld_packages'
    );

    $pid = wp_insert_post( $request );
    add_post_meta($pid, '_gfs_doc_id', $id, true);
    add_post_meta($pid, 'byp_dwnld_files', $document->wp_id);
    add_post_meta($pid, '_byp_dwnld_email_message', $message);

    $wpdb->query('START TRANSACTION');
    $result =
    $wpdb->update( 
        'byp_gf_gfs_documents', 
        array(
            'byp_dwnld_id' => $pid,
        ), 
        array( 'id' => intval($id) ), 
        array(
            '%d',
        ), 
        array( '%d' ) 
    );
    if($result) {
        $wpdb->query('COMMIT');
    }
    else {
        $wpdb->query('ROLLBACK');
    } 
}

function display_header()
{ ?>
    <style>
        .gfs_doc_container
        {
            width: 95%;
            max-width: 1200px;
            min-height: calc(100vh - 150px);
            margin: 0 auto;
            padding: 30px;
            background: #ffffff;
        }
        .gfs_doc_container .screen-header
        {
            width: 80%;
            text-align: center;
            border-bottom: 1px solid #dedede;
            margin: 0 auto 30px auto;
            padding-bottom: 10px;
        }
        .gfs_doc_container form
        {
            width: 400px;
            margin: 0 auto;
        }
    </style>
    <div class="gfs_doc_container">
<?php }

function display_footer()
{ ?>
    </div><!--/gfs_doc_container-->
<?php }

function display_deleted() {
    ?>
    <h4>Document Deleted</h4>
    <a class="button-primary" href="<?php echo site_url('wp-admin/admin.php?page=gfs-doc-manager') ?>">Return to Document Manager</a>
<?php }

function handle_document(){
    $pdf = $_FILES['test_upload_pdf'];

    // Use the wordpress function to upload
    // test_upload_pdf corresponds to the position in the $_FILES array
    // 0 means the content is not associated with any other posts
    $uploaded = media_handle_upload('test_upload_pdf', 0);

    // Error checking using WP functions
    if(is_wp_error($uploaded)){
        display_error();
    } else {
        storeDocument($uploaded);
    }
}

function storeDocument($id)
{
    global $wpdb;
    include 'pdfparser/vendor/autoload.php';
    // Get the file details
    $file = get_post($id);
    error_log(var_export($id,true));
    $filename = basename ( get_attached_file( $id ) );

    if ('application/pdf' === get_post_mime_type($id)) {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile(wp_get_attachment_url($id));
        $text = $pdf->getText();
        $excerpt = substr($text, 0, 300);
    } else {
        $text = $filename;
    }

    $wpdb->query('START TRANSACTION');
    $result1 = 
    $wpdb->insert( 
        'byp_gf_gfs_documents', 
        array( 
            'wp_id'         => $id,
            'text'          => $text,
            'excerpt'       => $excerpt,
            'title'         => $filename, 
            'created_at'    => date('Y-m-d G:i:s'),
            'updated_at'    => date('Y-m-d G:i:s'),
            'deleted_at'    => '0000-00-00 00:00:00'
        ), 
        array( 
            '%s',
            '%s',
            '%d',
            '%s',
            '%s',
            '%s'
        ) 
    );
    if($result1) {
        $wpdb->query('COMMIT');
        edit_document($wpdb->insert_id);
    }
    else {
        $wpdb->query('ROLLBACK');
        $wpdb->print_error();
        die();
        display_error();
    }
}

function display_error($action = 'processing')
{ ?>
    <div class="error">
        There was an error <?php echo $action ?> your document. Please refresh the page and try again.
    </div>
<?php }
function get_document($id) {
    global $wpdb;
    $query = "SELECT *  FROM `byp_gf_gfs_documents` WHERE `id` = ".$id;
    return $wpdb->get_row( $query );
}

function get_excerpt($document)
{
    return $document->excerpt;
    if (strlen($document->excerpt) < 1) {
        return substr($document->text, 0, 300);
    }
    return $document->excerpt;
}

function edit_document($id, $upload=false)
{
    global $wpdb;
    $query = "SELECT *  FROM `byp_gf_gfs_documents` WHERE `id` = ".$id;
    $document = $wpdb->get_row( $query );
    $wp_document = '';
    $title = $upload ? 'Upload Successful!' : 'Edit Document';
?>
<style type="text/css">
    .screen-cta {
        text-align: right;
    }
</style>

    <div class="screen-header">
    <h3 class="screen-cta"><a class="add_new" href="http://genforwardsurvey.com/core/wp-admin/admin.php?page=gfs-doc-manager">Upload A New Document</a></h3>
        <h1 class="screen-title"><?php echo $title; ?></h1>
        <h2 class="screen-directive">Use the form below to edit the file metadata for accurate search results.</h2>

    </div>
    <form method="post">
        <input type="hidden" name="gfs_doc-updated" value="<?php echo $id; ?>">
        <input type="hidden" name="gfs_doc-wp_file_id" value="<?php echo $document->wp_id; ?>">
        <table class="form-table">
            <tbody>
                <tr>
                    <th>
                        <label for="gfs_doc-title">Title</label>
                    </th>
                    <td>
                        <input type="text" id="gfs_doc-title" value="<?php echo $document->title; ?>" name="gfs_doc-title">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="gfs_doc-keyword">Keyword</label>
                    </th>
                    <td>
                        <input type="text" id="gfs_doc-keyword" value="<?php echo $document->keyword; ?>" name="gfs_doc-keyword">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="gfs_doc-excerpt">Excerpt</label>
                    </th>
                    <td>
                        <textarea id="gfs_doc-excerpt" value="" name="gfs_doc-excerpt"><?php echo $document->excerpt; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="gfs_doc-month">Month</label>
                    </th>
                    <td>
                        <select name="gfs_doc-month" id="gfs_doc-month" id="month" onchange="" size="1">
                            <option value="00" selected>Select a Month</option>
                            <option value="01" <?php echo $document->month == '01' ? 'selected' : ''; ?>>January</option>
                            <option value="02" <?php echo $document->month == '02' ? 'selected' : ''; ?>>February</option>
                            <option value="03" <?php echo $document->month == '03' ? 'selected' : ''; ?>>March</option>
                            <option value="04" <?php echo $document->month == '04' ? 'selected' : ''; ?>>April</option>
                            <option value="05" <?php echo $document->month == '05' ? 'selected' : ''; ?>>May</option>
                            <option value="06" <?php echo $document->month == '06' ? 'selected' : ''; ?>>June</option>
                            <option value="07" <?php echo $document->month == '07' ? 'selected' : ''; ?>>July</option>
                            <option value="08" <?php echo $document->month == '08' ? 'selected' : ''; ?>>August</option>
                            <option value="09" <?php echo $document->month == '09' ? 'selected' : ''; ?>>September</option>
                            <option value="10" <?php echo $document->month == '10' ? 'selected' : ''; ?>>October</option>
                            <option value="11" <?php echo $document->month == '11' ? 'selected' : ''; ?>>November</option>
                            <option value="12" <?php echo $document->month == '12' ? 'selected' : ''; ?>>December</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="gfs_doc-year">Year</label>
                    </th>
                    <td>
                        <select name="gfs_doc-year" id="gfs_doc-year" onchange="" size="1">
                            <option value="0000" selected>Select a Year</option>
                            <option value="2016" <?php echo $document->year == '2016' ? 'selected' : ''; ?>>2016</option>
                            <option value="2017" <?php echo $document->year == '2017' ? 'selected' : ''; ?>>2017</option>
              							<option value="2018" <?php echo $document->year == '2018' ? 'selected' : ''; ?>>2018</option>
              							<option value="2018" <?php echo $document->year == '2019' ? 'selected' : ''; ?>>2019</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="gfs_doc-type">Type</label>
                    </th>
                    <td>
                        <select name="gfs_doc-type" id="gfs_doc-type" onchange="" size="1">
                            <option value="null" selected>Select a Type</option>
                            <option value="codebook" <?php echo $document->type == 'codebook' ? 'selected' : ''; ?>>Codebook</option>
                            <option value="dataset" <?php echo $document->type == 'dataset' ? 'selected' : ''; ?>>Dataset</option>
                            <option value="factsheet" <?php echo $document->type == 'factsheet' ? 'selected' : ''; ?>>Factsheet</option>
                            <option value="field-report" <?php echo $document->type == 'field-report' ? 'selected' : ''; ?>>Field Report</option>
                            <option value="press-release" <?php echo $document->type == 'press-release' ? 'selected' : ''; ?>>Press Release</option>
                            <option value="report" <?php echo $document->type == 'report' ? 'selected' : ''; ?>>Report</option>
                            <option value="topline" <?php echo $document->type == 'topline' ? 'selected' : ''; ?>>Topline</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input name="submit" id="edit" class="button button-primary" value="Update" type="submit">
                        <a href="" id="deleteDocumentTrigger" class="button button-delete">Delete</a>
                    </td>
                </tr>
            </tbody>    
        </table>
    </form>
    <form id="deleteDocumentForm" method="post">
        <input type="hidden" name="gfs_doc-delete_id" value="<?php echo $document->id ?>">
        <input type="hidden" name="gfs_doc-delete" value="true">
    </form>

    <script type="text/javascript">
        jQuery(function($){
            $('#deleteDocumentTrigger').on('click', function(e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this document?')) {
                    $('#deleteDocumentForm').submit();
                }
            })
        })
    </script>
<?php
display_all();
}

function gfs_print_search_results($document)
{
    ?>
    <style type="text/css">
    .ui.items .item
    {
        margin-bottom: 55px;
        color: #565656;
    }
    </style>
        <div class="<?php echo strtolower($document->type) ?> y-<?php echo strtolower($document->year) ?> m-<?php echo strtolower($document->month) ?> item">
            <div class="ui tiny image doctype">
                <span class="day"><?php echo $document->type ?></span>
            </div>
            <div class="middle aligned content">
                <h3 class="header">
                    <?php echo $document->title ?>
                </h3>
                <p class="excerpt">
                    <?php echo get_excerpt($document); ?>
                </p>
                    <a href="<?php echo wp_get_attachment_url( $document->wp_id ); ?>" class="outboud-link">
                        <i class="external icon"></i>
                    </a>
            </div>
        </div>
<?php
}
