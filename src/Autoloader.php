<?php
class Autoloader {
    static public function loader($className) {
        $path = plugin_dir_path( __FILE__ );
        $filename = $path . str_replace("\\", '/', $className) . ".php";
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');