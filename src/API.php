<?php

use \GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;

class API
{
    public $client;

    private $token;

    private $baseURL;

    public function __construct()
    {
        $this->client = new Client();
        $this->baseURL = API_URL;
        $this->token = API_TOKEN;
    }

    public function get($filters = null, $bust = true)
    {
        $response = wp_cache_get('gfs-api-get' . $filters);
        
        if (false !== $response  && !$bust) {
            return $response;
        }
        $response = [];
        $uri = $this->baseURL . '/document/?token=' . $this->token;
        $uri = $filters ? $uri . '&types=' . $filters : $filters;
        try {
            $get = $this->client->get($uri);
            $response['body'] = json_decode($get->getBody(), true);
            $response['count'] = count($response['body']['documents']);
            $response['status'] = $get->getStatusCode();
            $response['success'] = true;
            wp_cache_set('gfs-api-get' . $filters, $response, 180);
        } catch (TransferException $e) {
            $response = [
                'status'    => $e->getCode(),
                'message'   => $e->getMessage(),
                'success'   => false,
            ];
        }
        return $response;
    }
    
    public function all($filters = null)
    {
        $response = [];
        $uri = $this->baseURL . '/document/?token=' . $this->token;
        $uri = $filters ? $uri . '&filters=' . $filters : $filters;
        try {
            $get = $this->client->get($uri);
            $response['body'] = json_decode($get->getBody(), true);
            $response['count'] = count($response['body']['documents']);
            $response['status'] = $get->getStatusCode();
            $response['success'] = true;
            wp_cache_add('gfs-api-all', $response);
        } catch (TransferException $e) {
            $response = [
                'status'    => $e->getCode(),
                'message'   => $e->getMessage(),
                'success'   => false,
            ];
            Log::error(var_export($response, true));
        }
        return $response;
    }


    public function post($url, $data)
    {
        $response = [];
        $data = [
            'form_params' => $data
        ];
        try {
            $request = $this->client->request('POST',$this->baseURL . '/'. $url .'/' . '?token=' . $this->token, $data);
            $response['body'] = json_decode($request->getBody(), true)['data'];
            $response['status'] = $request->getStatusCode();
            $response['success'] = true;
        } catch (GuzzleException $e) {
            $error = $e->getMessage();
            $response = [
                'status'    => $e->getCode(),
                'success'   => false,
                'message'   => $error,
            ];
        }
        return $response;
    }
}